#!/usr/bin/make -f

.PHONY: pics

FILENAME=nara-intro

all: $(FILENAME).pdf

%.pdf: %.ps
	ps2pdf $*.ps

%.ps: %.dvi
	dvips -o $*.ps $*.dvi

%.dvi: pics %.tex
	latex $*.tex

pics:
	make -C pics

clean: runclean
	make -C pics clean

runclean:
	rm -f *.dvi *.ps *.pdf *.aux *.log *~
